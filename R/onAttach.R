.onAttach <- function(libname, pkgname) {
    ## Runs when attached to search() path such as by library() or require()
    if (interactive()) {
        v = packageVersion("ctlcon")
        d = read.dcf(system.file("DESCRIPTION", package = "ctlcon"),
                     fields = c("Packaged", "Built"))
        if (is.na(d[1])) {
            if (is.na(d[2])) {
                return()
            } else {
                d = unlist(strsplit(d[2], split = "; "))[3]
            }
        } else {
            d = d[1]
        }

        dev = as.integer(v[1, 3]) %% 2 == 1 # version number odd
        packageStartupMessage("ctlcon ", v, if(dev) paste0(" IN DEVELOPMENT built ", d))
        if (dev && (Sys.Date() - as.Date(d)) > 28) {
            packageStartupMessage("**\nThis dev version of ctlcon was built more than 4 weeks ago, check updates\n**")
        }
        packageStartupMessage("For help type ?ctlcon")
##        packageStartupMessage("Interested in learning more R? Check out: https://www.datacamp.com/courses/cbb-jamboree-intro-to-r/")
    }
}
