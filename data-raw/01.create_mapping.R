## Create mapping with dataset parameters
library(tidyverse)
library(devtools)

## Read in mapping; add human readable ID -----------------------
## mapping <- read_tsv('data-raw/misc/annotation_preprocessing/ctlcon_data_annotation.tsv')
mapping <- readr::read_tsv('data-raw/mapping_GSM-added.txt') %>%
    mutate(cell = fct_relevel(cell, c("N", "E", "MP", "TE", "M", "S"))) %>%
    dplyr::arrange(cell, assay, lab, rep)

devtools::use_data(mapping, compress = 'xz', overwrite = TRUE)

