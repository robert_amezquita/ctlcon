# ctlcon: data-raw

This folder assumes a connection to the data repository, called here `AdvSeq`, created by the workflows defined in the repository `seqsnake`. Primarily, this data repository should contain the collection of bigwigs, macs2 peak calls, and final bam files that will be used for further downstream analysis. 

* `annotation_preprocessing/`
  * Contains SRA run tables and manual annotation information of various sequencing datasets
  * Script `process_annotation.R` munges the various tables into a final output, `data_annotation.tsv`
* `data_annotation.tsv`
  * Final output of the `annotation_preprocessing/` munging
  * Contains essential desciprotrs pertaining to all datasets used in this package
* `write_configs.R`
  * Writes config files containing necessary information needed to run the workflows in the processing repository `seqsnake`
