library(reppeak)
library(tidyverse)
load('data/narrowpeak.rda')
load('data/broadpeak.rda')

## Consensus Peaksets by Peak-calls (narrowpeak)
## Add annotation and group for combining by consensus across lab reps
dat <- narrowpeak %>%
    inner_join(mapping[, c('ID', 'cell', 'assay')], ., by = 'ID') %>%
    filter(cell != 'S') %>%
    unnest() %>%
    reppeak::center_summits(window = 250) %>%
    group_by(cell, assay) %>%
    nest() %>%
    filter(assay != 'H3K4me1')

## Repeat procedure for broadpeak calls
dat2 <- broadpeak %>%
    inner_join(mapping[, c('ID', 'cell', 'assay')], ., by = 'ID') %>%
    filter(cell != 'S') %>%
    unnest() %>%
    group_by(cell, assay) %>%
    nest()

## Bind both narrow/broad datasets
datf <- bind_rows(dat, dat2)

## Calculate consensus intervals
consensuspeak <- datf %>%
    mutate(data = map(data, reppeak::consensus_intervals))
names(consensuspeak$data) <- consensuspeak$assay

## Calculating consensus peaks per assay across cell types
## Remove effector as size distribution of sites is odd
assaypeak <- consensuspeak %>%
    filter(cell != 'E') %>%
    unnest() %>%
    group_by(assay) %>%
    valr::bed_merge() %>%
    nest()
names(assaypeak$data) <- assaypeak$assay

## Assign peak IDs to both datasets
.assign_consensus_peakid <- function(data, cell, assay) {
    data %>%
        mutate(peak_id = paste0(cell, '_', assay, '_peak_', row_number()))
}

.assign_assay_peakid <- function(data, assay) {
    data %>%
        mutate(peak_id = paste0(assay, '_peak_', row_number()))
}

consensuspeak$data <- pmap(.l = list(data = consensuspeak$data,
               cell = consensuspeak$cell,
               assay = consensuspeak$assay),
     .assign_consensus_peakid)

assaypeak$data <- pmap(.l = list(data = assaypeak$data,
                                 assay = assaypeak$assay),
                       .assign_assay_peakid)

## Save data
devtools::use_data(assaypeak, consensuspeak, compress = 'xz', overwrite = TRUE)

